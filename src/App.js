import React, { Component } from 'react';
import { Route, Link, Router, Switch, } from 'react-router-dom'
import { createBrowserHistory } from 'history';
import Menu from './components/pages/menu';
import Home from './components/pages/home';
import SelectDate from './components/pages/home/select';
import RoomSelect from './components/pages/home/room';
import PersonalDetail from './components/pages/home/personal';
import Payments from './components/pages/payment';
import ConfirmOrder from './components/pages/confirm'
import './App.css';
import './components/assets/fonts/css/all.css';
import './components/assets/fonts/css/fontawesome.min.css';
import TimeCountdown from './components/pages/home/content/timeCountdown'

const history = createBrowserHistory();

history.listen(_ => {
  window.scrollTo(0, 0)  
})

class App extends Component {

 
  render() {
    // const pathUrl = window.location.pathname;
    // console.log(pathUrl, '---menu')

    return (
      <div className="App">
        {/* {
              pathUrl === '/' ? (
                null
              ) : (<Menu pathUrl={pathUrl} />)
            } */}
        <Router  history={history}>
          <div>
            <Route exact path="/" component={Home} />
            <Route exact path="/select_date" component={SelectDate} />
            <Route exact path="/room_select" component={RoomSelect} />
            <Route exact path="/personal_detail" component={PersonalDetail} />
            <Route exact path="/payment" component={Payments} />
            <Route exact path="/confirm" component={ConfirmOrder} />
            <Route exact path="/countdown" component={TimeCountdown} />
          </div>
        </Router>
      </div>
    )
  }
}


export default App;
