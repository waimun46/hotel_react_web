const homepage = [
  {
    cover: 'https://az760333.vo.msecnd.net/-/media/property/jdv/vitale/vitale-guestroom-king-03-pr-pd0912-crpd1600x760.jpg?ts=326a7836-56d6-407f-b5ef-233fb7eb4c36',
    price: {
      1: ['RM180'], 2: ['RM180'], 3: ['RM120'], 4: ['RM280'], 5: ['RM180'], 6: ['RM180'], 7: ['RM180'],
      8: ['RM180'], 9: ['RM190'], 10: ['RM180'], 11: ['RM180'], 12: ['RM380'], 13: ['RM480'], 14: ['RM280'],
    }
  },

]


const code = [
  { code_id: 'MYR', room: '1' }, { code_id: 'EUR', room: '2' }, { code_id: 'USD', room: '4' }, { code_id: 'CAD', room: '5' },
];

const logo = [
  { logo: 'https://www.issaquahhighlands.com/wp-content/uploads/2015/10/bed.jpg' }
]

const country = [
  { country: 'Malaysa' }, { country: 'Australia' }, { country: 'USA' }, { country: 'HongKong' }, { country: 'Taiwan' }, { country: 'Philippines' },
  { country: 'Singapore' }, { country: 'Thailand' }, { country: 'Turkey' }, { country: 'Switzerland' }, { country: 'Russia' }, { country: 'Portugal' },
]

const designations = [
  { designation: 'Mr.' }, { designation: 'Mrs.' }, { designation: 'Ms.' }, { designation: 'Miss.' },
]


const dataRoom = [
  {
    roomImg: [
      { img: 'https://qtxasset.com/hotelmanagement/1551371746/Hotel-Ottilia-Suite_bedroom.jpg?CTPVy_Pq8BRc23xoRoFDidu8SDjVCBBO', },
      { img: 'http://filcultural.info/wp-content/uploads/2019/03/modern-bedroom-designs-2019.jpg', },
      { img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSwWgK4uA_-lDJJlpXtSsl_tJrijPbTE1cC8c0chik46elfSTE5', }
    ],
    type: 'Standard Queen',
    room_left: '5',
    facilitie: [
      { bed: '1 double bed' }, { view: 'City View' }, { food: 'Breakfast' }, { person: 'Max 2' }, { wifi: 'Free WIFI' }, { smook: 'Non-smoking' }
    ],
    description: '1 Queen bed, personal toiletries, free wifi, LCD TV, selected Astro channel, complimentary coffee and tea facilities, breakfast included.',
    price: '180',
    room_length: [
      { lenght: '1' }, { lenght: '2' }, { lenght: '3' }, { lenght: '4' }, { lenght: '5' },
    ],
    guest: '2',
    duration: '1 Night',
    bed_type: '1 double bed',
  },
  {
    roomImg: [
      { img: 'https://www.businessinsider.in/thumb/msid-68664363,width-600,resizemode-4/Heres-why-hotel-room-rates-in-India-may-double-in-the-next-3-to-4-years.jpg?225157', },
      { img: 'https://www.lemontreehotels.com/getattachment/9224ce08-7132-4509-87f9-28293c769bc8/Executive-Room.aspx', },
      { img: 'https://t-ec.bstatic.com/images/hotel/max1024x768/159/159937900.jpg', }
    ],
    type: 'Family Room',
    room_left: '3',
    facilitie: [
      { bed: '2 Single Beds' }, { view: 'City View' }, { food: 'Breakfast' }, { person: 'Max 2' }, { wifi: 'Free WIFI' }, { smook: 'Non-smoking' }
    ],
    description: '2 Queen and 1 Single bed, personal toiletries, free wifi, LCD TV, selected Astro channel, complimentary coffee and tea facilities, breakfast included.',
    price: '280',
    room_length: [
      { lenght: '1' }, { lenght: '2' }, { lenght: '3' }, { lenght: '4' }, { lenght: '5' },
    ],
    guest: '1',
    duration: '1 Night',
    bed_type: '2 double bed',
  },
  {
    roomImg: [
      { img: 'https://www.hotelnikkosf.com/resourcefiles/roomssmallimages/hotel-nikko-san-francisco-deluxe-room-th.jpg', },
      { img: 'http://www.hotelroomsearch.net/im/hotels/us/hotel-nikko-san-francisco-4.jpg', },
      { img: 'https://media-cdn.tripadvisor.com/media/photo-s/13/ee/a4/6f/deluxe-two-doubles-v18423905.jpg', }
    ],
    type: 'Superior Twin Room',
    room_left: '8',
    facilitie: [
      { bed: '2 Single Beds' }, { view: 'City View' }, { food: 'Breakfast' }, { person: 'Max 2' }, { wifi: 'Free WIFI' }, { smook: 'Non-smoking' }
    ],
    description: '2 Queen and 1 Single bed, personal toiletries, free wifi, LCD TV, selected Astro channel, complimentary coffee and tea facilities, breakfast included.',
    price: '480',
    room_length: [
      { lenght: '1' }, { lenght: '2' }, { lenght: '3' }, { lenght: '4' }, { lenght: '5' },
    ],
    guest: '4',
    duration: '1 Night',
    bed_type: '2 double bed',
  },
  {
    roomImg: [
      { img: 'https://www.hotelnikkosf.com/resourcefiles/roomssmallimages/hotel-nikko-san-francisco-deluxe-room-th.jpg', },
      { img: 'http://www.hotelroomsearch.net/im/hotels/us/hotel-nikko-san-francisco-4.jpg', },
      { img: 'https://media-cdn.tripadvisor.com/media/photo-s/13/ee/a4/6f/deluxe-two-doubles-v18423905.jpg', }
    ],
    type: 'Superior Twin Room',
    room_left: '8',
    facilitie: [
      { bed: '2 Single Beds' }, { view: 'City View' }, { food: 'Breakfast' }, { person: 'Max 2' }, { wifi: 'Free WIFI' }, { smook: 'Non-smoking' }
    ],
    description: '2 Queen and 1 Single bed, personal toiletries, free wifi, LCD TV, selected Astro channel, complimentary coffee and tea facilities, breakfast included.',
    price: '480',
    room_length: [
      { lenght: '1' }, { lenght: '2' }, { lenght: '3' }, { lenght: '4' }, { lenght: '5' },
    ],
    guest: '1',
    duration: '1 Night',
    bed_type: '1 double bed',
  },
  {
    roomImg: [
      { img: 'https://www.hotelnikkosf.com/resourcefiles/roomssmallimages/hotel-nikko-san-francisco-deluxe-room-th.jpg', },
      { img: 'http://www.hotelroomsearch.net/im/hotels/us/hotel-nikko-san-francisco-4.jpg', },
      { img: 'https://media-cdn.tripadvisor.com/media/photo-s/13/ee/a4/6f/deluxe-two-doubles-v18423905.jpg', }
    ],
    type: 'Superior Twin Room',
    room_left: '8',
    facilitie: [
      { bed: '2 Single Beds' }, { view: 'City View' }, { food: 'Breakfast' }, { person: 'Max 2' }, { wifi: 'Free WIFI' }, { smook: 'Non-smoking' }
    ],
    description: '2 Queen and 1 Single bed, personal toiletries, free wifi, LCD TV, selected Astro channel, complimentary coffee and tea facilities, breakfast included.',
    price: '480',
    room_length: [
      { lenght: '1' }, { lenght: '2' }, { lenght: '3' }, { lenght: '4' }, { lenght: '5' },
    ],
    guest: '3',
    duration: '1 Night',
    bed_type: '2 double bed',
  }
]

const summary = [
  {
    duration: '1 Night', room_type: 'Standard Queen', bed_type: '1 double bed', no_room: '1 Room', guest: '2 Guest',
    provide: '1 breakfast,WIFI', price: 'RM 180', add_bed: 'RM 50', add_breakfast: 'RM 50', total: '280',
    other: [
      { logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Expedia_2012_logo.svg/1280px-Expedia_2012_logo.svg.png', price: '200' },
      { logo: 'https://upload.wikimedia.org/wikipedia/commons/0/0c/Agoda_logo.png', price: '300' },
      { logo: 'http://www.pngnames.com/files/4/Booking-Logo-PNG-Photo.png', price: '400' },
    ]
  }
]

export { dataRoom, code, logo, summary, country, designations , homepage};
