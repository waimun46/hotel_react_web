import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { InputNumber, Button, Menu, Dropdown, Icon, Calendar, Badge } from 'antd';
import { Image } from 'semantic-ui-react'
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import FontAwesome from 'react-fontawesome';
import moment from 'moment';
import 'moment/min/locales';
import { homepage } from '../../json';
import logo from '../../assets/logo/logo.png';
import './home.css';



class SelectDate extends Component {
  static defaultProps = {
    numberOfMonths: 1,
  };

  constructor(props) {
    super(props);
    this.state = {
      adult: 1,
      children: 0,
      infants: 0,
      visible: false,
      from: null,
      to: null,
      enteredTo: null,
      price: null,
      month: null
    }
    this.handleDayClick = this.handleDayClick.bind(this);
    this.handleDayMouseEnter = this.handleDayMouseEnter.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.renderPrice = this.renderPrice.bind(this);
    
  }

  /************************************************* select date function **************************************************/
  isSelectingFirstDay(from, to, day) {
    const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
    const isRangeSelected = from && to;
    return !from || isBeforeFirstDay || isRangeSelected;
  }
  handleDayClick(day) {
    const { from, to, price } = this.state;
    if (from && to && day >= from && day <= to) {
      this.handleResetClick();
      return;
    }
    if (this.isSelectingFirstDay(from, to, day)) {
      this.setState({ from: day, to: null, enteredTo: null, });
    } else {
      this.setState({ to: day, enteredTo: day, });
    }
  }
  handleDayMouseEnter(day) {
    const { from, to } = this.state;
    if (!this.isSelectingFirstDay(from, to, day)) {
      this.setState({ enteredTo: day, });
    }
  }
  handleResetClick() {
    this.setState({ from: null, to: null, enteredTo: null, adult: 1, children: 0, infants: 0, });
  }

  /************************************************* dropdown handel **************************************************/
  handleMenuClick = e => {
    if (e.key === '3') {
      this.setState({ visible: false });
    }
  };

  handleVisibleChange = flag => {
    this.setState({ visible: flag });
  };

  /************************************************* count adult **************************************************/
  increaseAdult = () => {
    let adult = this.state.adult + 1;
    if (adult > 10) { adult = 10; }
    this.setState({ adult });
  };

  declineAdult = () => {
    let adult = this.state.adult - 1;
    if (adult < 1) { adult = 1; }
    this.setState({ adult });
  };

  /************************************************* count children **************************************************/
  increaseChildren = () => {
    let children = this.state.children + 1;
    if (children > 10) { children = 10; }
    this.setState({ children });
  };

  declineChildren = () => {
    let children = this.state.children - 1;
    if (children < 0) { children = 0; }
    this.setState({ children });
  };

  /************************************************* count infants **************************************************/
  increaseInfants = () => {
    let infants = this.state.infants + 1;
    if (infants > 10) { infants = 10; }
    this.setState({ infants });
  };

  declineInfants = () => {
    let infants = this.state.infants - 1;
    if (infants < 0) { infants = 0; }
    this.setState({ infants });
  };

  /************************************************* data price display function **************************************************/
  renderPrice(day) {
    const date = day.getDate();
    const cellStyle = {
      position: 'relative',
    };
    const priceData = homepage[0].price;
    return (
      <div style={cellStyle}>
        <div >{date}</div>
        {priceData[date] &&
         priceData[date].map((item, i) => (
            <div key={i} className="priceStyle">
              {item}
            </div>
          ))}
      </div>
      
    );
  
  }





  render() {
    const { from, to, enteredTo, adult, children, infants, visible } = this.state;
    const modifiers = { start: from, end: enteredTo,  };
    const disabledDays = { before: this.state.from };
    const selectedDays = [from, { from, to: enteredTo }];

    // console.log(homepage,'----homepage')


    // console.log('date----from', moment(from).format('DD-MM-YYYY') )
    console.log('date----to', moment(to).format('DD-MM-YYYY') )
    console.log('renderPrice----to', this.state.price)


    const menu = (
      <Menu onClick={this.handleMenuClick} className="drop_warp">
        <Menu.Item key="0" className="menu_drop">
          <FontAwesome name='male' className="fa_icon adults" />
          <span className="btn_warp">
            <Button onClick={this.declineAdult} icon="minus" />
            <span className="select_num">{adult}</span>
            <Button onClick={this.increaseAdult} icon="plus" />
          </span>
        </Menu.Item>
        <Menu.Item key="1" className="menu_drop">
          <FontAwesome name='child' className="fa_icon children" />
          <span className="btn_warp">
            <Button onClick={this.declineChildren} icon="minus" />
            <span className="select_num">{children}</span>
            <Button onClick={this.increaseChildren} icon="plus" />
          </span>
        </Menu.Item>
        <Menu.Item key="2" className="menu_drop">
          <FontAwesome name='baby' className="fa_icon infants" />
          <span className="btn_warp">
            <Button onClick={this.declineInfants} icon="minus" />
            <span className="select_num">{infants}</span>
            <Button onClick={this.increaseInfants} icon="plus" />
          </span>
        </Menu.Item>
      </Menu>
    );


    return (
      <div className="SelectDate">
        <Image src={homepage[0].cover} fluid />
        <div className="overlay"></div>
        <div className="content">
          <div className="warpper">
          <Image src={logo} className="logo" />
            <h1>Hotel Seri Malaysia Melaka</h1>
            <div className="date_warpper">
              <div className="date_select_content">
                <div className="warp">
                  <p>CHECK IN</p>
                  <p className="select">
                    {!from && 'Select first day'}
                    {from && `${from.toLocaleDateString()}`}
                  </p>
                </div>
                <div className="warp">
                  <p>CHECK OUT</p>
                  <p className="select">
                    {!to && 'Select last day'}
                    {to && `${to.toLocaleDateString()}`}
                  </p>
                </div>
                <div className="warp">
                  <p>GUEST</p>
                  <p className="number_input">
                    <Dropdown overlay={menu} trigger={['click']} visible={visible}
                      onVisibleChange={this.handleVisibleChange} className="dropdown_select">
                      <div className="ant-dropdown-link">
                        <span className="warp_select adults">
                          <FontAwesome name='male' className="fa_icon adults" />{adult}
                        </span>
                        <span className="warp_select children">
                          <FontAwesome name='child' className="fa_icon children" />{children}
                        </span>
                        <span className="warp_select infants">
                          <FontAwesome name='baby' className="fa_icon infants" />{infants}
                        </span>
                        <Icon type="down" />
                      </div>
                    </Dropdown>
                  </p>
                </div>
                <div className="clear"></div>
              </div>
              <DayPicker
                className="Range"
                numberOfMonths={1}
                fromMonth={from}
                selectedDays={selectedDays}
                disabledDays={disabledDays}
                onDayClick={this.handleDayClick}
                onDayMouseEnter={this.handleDayMouseEnter}
                modifiers={modifiers}
                renderDay={this.renderPrice}
              />
            </div>
            <div className="select_btn_warp">
            {/* <Link to="/room_select" onClick={() => window.location.refresh()}> */}
              <Link to="/room_select">
                <Button><FontAwesome name='long-arrow-alt-right'/></Button>
              </Link>
              <Button onClick={this.handleResetClick}><FontAwesome name='redo'/></Button>
            </div>
            <div className="clear"></div>

          </div>
        </div>
      </div>
    )
  }
}


export default withRouter(SelectDate);
