import React, { Component } from 'react';
import RoomDetail from './content/roomDetial';
import Summary from './content/summary';
import { Icon, Image, List } from 'semantic-ui-react'
import { InputNumber, Select, Collapse, Menu, Dropdown, Button, Drawer, } from 'antd';
import FontAwesome from 'react-fontawesome';
import Slider from "react-slick";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { dataRoom, code, logo, summary } from '../../json';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import TimeCountdown from './content/timeCountdown';
import BookingDetails from './content/bookingDetail';
import OtherDeals from './content/otherDeal';
import Menus from '../menu';

const { Option } = Select;
const { Panel } = Collapse;
const listData = [1, 2, 3];


class RoomSelect extends Component {

  constructor(props) {
    super(props);
    this.state = {
      windowHeight: undefined,
      windowWidth: undefined,
      from: null,
      to: null,
      enteredTo: null,
      adult: 1,
      children: 0,
      infants: 0,
      visible: false,
      visibleDrawer: false,
      placement: 'bottom',
      addlist: [{
        room: dataRoom[0].room_left, type: dataRoom[0].type,
        price: dataRoom[0].price, duration: dataRoom[0].duration, guest: dataRoom[0].guest,
        bed_type: dataRoom[0].bed_type,
      }],
    }
    this.handleDayClick = this.handleDayClick.bind(this);
    this.handleDayMouseEnter = this.handleDayMouseEnter.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
  }


  /************************************************* Drawer function in responsive **************************************************/
  showDrawer = () => {
    this.setState({
      visibleDrawer: true,
    });
  };

  onCloseDrawer = () => {
    this.setState({
      visibleDrawer: false,
    });
  };

  onChangeDrawer = e => {
    this.setState({
      placement: e.target.value,
    });
  };


  /************************************************* calander window width function **************************************************/
  handleResize = () => this.setState({
    windowHeight: window.innerHeight,
    windowWidth: window.innerWidth
  });

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  /************************************************* calander function **************************************************/
  isSelectingFirstDay(from, to, day) {
    const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
    const isRangeSelected = from && to;
    return !from || isBeforeFirstDay || isRangeSelected;
  }
  handleDayClick(day) {
    const { from, to } = this.state;
    if (from && to && day >= from && day <= to) {
      this.handleResetClick();
      return;
    }
    if (this.isSelectingFirstDay(from, to, day)) {
      this.setState({
        from: day,
        to: null,
        enteredTo: null,
      });
    } else {
      this.setState({
        to: day,
        enteredTo: day,
      });
    }
  }
  handleDayMouseEnter(day) {
    const { from, to } = this.state;
    if (!this.isSelectingFirstDay(from, to, day)) {
      this.setState({
        enteredTo: day,
      });
    }
  }
  handleResetClick() {
    this.setState({ from: null, to: null, enteredTo: null });
  }

  /************************************************* count menu **************************************************/
  handleMenuClick = e => {
    if (e.key === '3') {
      this.setState({ visible: false });
    }
  };

  handleVisibleChange = flag => {
    this.setState({ visible: flag });
  };


  /************************************************* count adult **************************************************/
  increaseAdult = () => {
    let adult = this.state.adult + 1;
    if (adult > 10) { adult = 10; }
    this.setState({ adult });
  };

  declineAdult = () => {
    let adult = this.state.adult - 1;
    if (adult < 1) { adult = 1; }
    this.setState({ adult });
  };

  /************************************************* count children **************************************************/
  increaseChildren = () => {
    let children = this.state.children + 1;
    if (children > 10) { children = 10; }
    this.setState({ children });
  };

  declineChildren = () => {
    let children = this.state.children - 1;
    if (children < 0) { children = 0; }
    this.setState({ children });
  };

  /************************************************* count infants **************************************************/
  increaseInfants = () => {
    let infants = this.state.infants + 1;
    if (infants > 10) { infants = 10; }
    this.setState({ infants });
  };

  declineInfants = () => {
    let infants = this.state.infants - 1;
    if (infants < 0) { infants = 0; }
    this.setState({ infants });
  };

  /************************************************* handleChangeSelect function **************************************************/
  handleChangeSelect(value) {
    console.log(`selected ${value}`);
  }

  /************************************************* add list **************************************************/
  addToSummary(item, num) {
    console.log('add')
    let newAddList = {
      room: num.lenght, type: item.type,
      price: item.price, duration: item.duration, guest: item.guest,
      bed_type: item.bed_type,
    }
    this.setState({
      addlist: [...this.state.addlist, newAddList],
    });
  };

  removeSummaryList = index => () => {
    this.setState({
      addlist: this.state.addlist.filter((s, sidx) => index !== sidx)
    });
  };





  render() {
    // console.log(dataRoom, '-----dataRoom')

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false
    };
    const { from, to, enteredTo, visible, adult, children, infants } = this.state;
    const modifiers = { start: from, end: enteredTo };
    const disabledDays = { before: this.state.from };
    const selectedDays = [from, { from, to: enteredTo }];

    const menu = (
      <Menu onClick={this.handleMenuClick} className="drop_warp">
        <Menu.Item key="0" className="menu_drop">
          <FontAwesome name='male' className="fa_icon adults" />
          <span className="btn_warp">
            <Button onClick={this.declineAdult} icon="minus" />
            <span className="select_num">{adult}</span>
            <Button onClick={this.increaseAdult} icon="plus" />
          </span>
        </Menu.Item>
        <Menu.Item key="1" className="menu_drop">
          <FontAwesome name='child' className="fa_icon children" />
          <span className="btn_warp">
            <Button onClick={this.declineChildren} icon="minus" />
            <span className="select_num">{children}</span>
            <Button onClick={this.increaseChildren} icon="plus" />
          </span>
        </Menu.Item>
        <Menu.Item key="2" className="menu_drop">
          <FontAwesome name='baby' className="fa_icon infants" />
          <span className="btn_warp">
            <Button onClick={this.declineInfants} icon="minus" />
            <span className="select_num">{infants}</span>
            <Button onClick={this.increaseInfants} icon="plus" />
          </span>
        </Menu.Item>
      </Menu>
    );


    return (
      <div>
        <Menus />

        <div className="RoomSelect">

          {/* <RoomDetail /> */}
          <div className="res_warp">
            <div className="RoomDetail">
              <div className="content margin_content">

                <div className="info">
                  <p>
                    <span>THU, Apr 4</span> to <span>FRI, Apr 5</span>|<span>1 Night</span>|
                  <span className="guest_select">
                      <Icon name="user" />
                      <Dropdown
                        overlay={menu}
                        onVisibleChange={this.handleVisibleChange}
                        visible={visible}
                        className="dropdown_select"
                      >
                        <span className="ant-dropdown-link" href="#">
                          <span className="warp_select adults">
                            <FontAwesome name='male' className="fa_icon adults" />{adult}
                          </span>
                          <span className="warp_select children">
                            <FontAwesome name='child' className="fa_icon children" />{children}
                          </span>
                          <span className="warp_select infants">
                            <FontAwesome name='baby' className="fa_icon infants" />{infants}
                          </span>
                          <Icon name="chevron down" />
                        </span>
                      </Dropdown>

                      {/* <button
                      type="button"
                      onClick={() => this.addToSummary()}
                      className="small"
                    >
                      addToSummary
                    </button> */}

                      {/* {this.state.addlist.map((item, idx) => (
                      <div className="shareholder">
                       <p>dddddd</p>
                        <button
                          type="button"
                          onClick={this.removeSummaryList(idx)}
                          className="small"
                        >
                          -
                        </button>
                      </div>
                    ))
                    } */}



                    </span>
                    <span className="myr">
                      <Select defaultValue="MYR" className="select" onChange={this.handleSelectChange}>
                        {
                          code.map((item, i) => {
                            return (
                              <Option value={item.code_id}>{item.code_id}</Option>
                            )
                          })
                        }
                      </Select>
                    </span>
                    <div className="clear"></div>
                  </p>
                  <Collapse accordion Icon={false} bordered={false} expandIconPosition="right">
                    <Panel header="Change Date" key="1">
                      <DayPicker
                        className="Range"
                        numberOfMonths={this.state.windowWidth > 750 ? 2 : 1}
                        fromMonth={from}
                        selectedDays={selectedDays}
                        disabledDays={disabledDays}
                        modifiers={modifiers}
                        onDayClick={this.handleDayClick}
                        onDayMouseEnter={this.handleDayMouseEnter}
                      />
                      <div>
                        {!from && !to && 'Please select the first day.'}
                        {from && !to && 'Please select the last day.'}
                        {from &&
                          to &&
                          `Selected from ${from.toLocaleDateString()} to
                    ${to.toLocaleDateString()}`}{' '}
                        {from &&
                          to && (
                            <Button className="link" className="date_reset" onClick={this.handleResetClick}>
                              Reset
                          </Button>
                          )}
                      </div>
                    </Panel>
                  </Collapse>
                </div>

                <div className="res_other_deals">
                  <OtherDeals />
                </div>



                {
                  dataRoom.map((item, index) => {
                    return (
                      <div className="list_content" key={index}>
                        <div className="slider">
                          <div className="slider_warp">
                            <Slider {...settings}>
                              {
                                item.roomImg.map((item) => {
                                  return (
                                    <div>
                                      <img src={item.img} alt="img" />
                                    </div>
                                  )
                                })
                              }
                            </Slider>
                            <div className="notice">Left {item.room_left}</div>
                          </div>
                        </div>

                        <div className="content_warp">
                          <p className="title">{item.type}</p>

                          <List horizontal relaxed >
                            <List.Item>
                              <Image avatar src={logo[0].logo} />
                              <List.Content>
                                <List.Header as='a'>{item.facilitie[0].bed}</List.Header>
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <Image avatar src={logo[0].logo} />
                              <List.Content>
                                <List.Header as='a'>{item.facilitie[1].view}</List.Header>
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <Image avatar src={logo[0].logo} />
                              <List.Content>
                                <List.Header as='a'>{item.facilitie[2].food}</List.Header>
                              </List.Content>
                            </List.Item>
                          </List>

                          <List horizontal relaxed >
                            <List.Item>
                              <Image avatar src={logo[0].logo} />
                              <List.Content>
                                <List.Header as='a'>{item.facilitie[3].person}</List.Header>
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <Image avatar src={logo[0].logo} />
                              <List.Content>
                                <List.Header as='a'>{item.facilitie[4].wifi}</List.Header>
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <Image avatar src={logo[0].logo} />
                              <List.Content>
                                <List.Header as='a'>{item.facilitie[5].smook}</List.Header>
                              </List.Content>in
                            </List.Item>
                          </List>

                          <p className="detial_content">{item.description}</p>

                          <div className="select_content">
                            <span className="price"><small>MYR</small><span>{item.price}</span></span>

                            <span className="room_select">Room</span>
                            <Select defaultValue="1" onChange={this.handleSelectChange} style={{ width: 80 }} >
                              {
                                item.room_length.map((num, i) => {
                                  return (
                                    <Option value={num.lenght} key={i} onClick={this.addToSummary.bind(this, item, num)}>{num.lenght}</Option>
                                  )
                                })
                              }
                            </Select>
                          </div>

                          <div className="clear"></div>

                        </div>

                      </div>

                    )
                  })
                }


                <div className="clear"></div>


              </div>
            </div>
          </div>

          {/************************************************* NO RESPONSIVE **************************************************/}
          {
            this.state.windowWidth < 1000 ? (
              null
            ) : (
                <div className="desktop_view">
                  <Summary selectData={this.state.addlist} onClickRemove={this.removeSummaryList} />
                </div>
              )
          }

          {/************************************************* RESPONSIVE **************************************************/}
          {
            this.state.windowWidth > 1000 ? (
              null
            ) : (
                <div className="mobile_view stick_bottom" >
                  <Button type="primary" onClick={this.showDrawer}>
                    <span className="details">
                      <p>RM 180 for 2 guest</p>
                      <p>See details</p>
                    </span>
                    <span className="time">
                      <TimeCountdown drawer={this.onCloseDrawer} />
                    </span>
                  </Button>
                  <Drawer
                    placement={this.state.placement}
                    onClose={this.onCloseDrawer}
                    visible={this.state.visibleDrawer}
                    height="90%"
                    className="drawer_warp"
                  >
                    <div className="res_drawer">
                      <BookingDetails summary={summary[0]} data={this.state.addlist} onClickRemove={this.removeSummaryList} />
                    </div>
                  </Drawer>
                </div>
              )
          }





          <div className="clear"></div>
        </div>
      </div>
    )
  }
}


export default RoomSelect;
