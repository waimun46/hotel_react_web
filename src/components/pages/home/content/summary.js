import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { summary } from '../../../json';
import OtherDeals from './otherDeal';
import TimeCountdown from './timeCountdown';
import BookingDetails from './bookingDetail';



class Summary extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sticky: 10,
      data: [],
    }

  }

  componentDidUpdate() {
    if (this.props.selectData !== this.state.data) {
      this.setState({
        data: this.props.selectData
      })
    }

  }


  render() {

    //console.log(this.props.onClickRemove, '---onClickRemove');

    return (
      <div className="Summery">

        <TimeCountdown />

        <OtherDeals />

        <BookingDetails summary={summary[0]} data={this.state.data} onClickRemove={this.props.onClickRemove}/>


      </div>
    )
  }
}


export default withRouter(Summary);
