import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Card, Checkbox, Affix } from 'antd';
import { summary } from '../../../json';

class PersonalBookingDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sticky: 10,
    }

  }

  onChangeCheckbox(e) {
    console.log(`checked = ${e.target.checked}`);
  }


  render() {
    const { sticky } = this.state;
    //  window.location.refresh()

    return (
      <div className="PersonalBookingDetail">
        <Affix offsetTop={sticky}>
          <Card className="card_warp"
            actions={[
              <div onClick={this.props.submitFunction}>
                <div className="footer">CONFIRM & PAY</div>
              </div>
            ]}>
            <div className="booking_detail">
              <p><span className="content_title">Check In</span><span className="content_list">Thu,Apr 4</span></p>
              <p><span className="content_title">Check Out</span><span className="content_list">Fri,Apr 5</span></p>
            </div>
            <div className="booking_detail">
              <p><span className="content_title">Duration of Stay</span><span className="content_list">{summary[0].duration}</span></p>
              <p><span className="content_title">Room Type</span><span className="content_list">{summary[0].room_type}</span></p>
              <p><span className="content_title">Bed Type</span><span className="content_list">{summary[0].bed_type}</span></p>
              <p><span className="content_title">No of Rooms</span><span className="content_list">{summary[0].no_room}</span></p>
              <p><span className="content_title">Guest No</span><span className="content_list">{summary[0].guest}</span></p>
              <p><span className="content_title">Price</span><span className="content_list">{summary[0].price}</span></p>
            </div>
            <div className="facilitie">
              <div className="other_warp">
                <p className="content_title">Other charges : </p>
                <div className="other_detail">
                  <p><span className="content_title">Add Bed</span><span className="content_list">{summary[0].add_bed}</span></p>
                  <p><span className="content_title">Add Breakfast</span><span className="content_list">{summary[0].add_breakfast}</span></p>
                </div>
              </div>
            </div>
            <div className="summary_price">
              <p><span className="content">SUMMARY</span><span className="price"><small>RM</small>{summary[0].total}</span></p>
            </div>
            <div className="notice">
              <p className="content">
                Price Included: 6% Service Tax, if any
                Price Not Included: RM 10 Tourism Tax per room per night (foreign guest only, not applicable to Malaysians),
                RM 2 Heritage Tax per room per night (if any)
                Full Payment Required Upon Booking.
              <p><a href="#">Cancellation Policy</a></p>
                <p><a href="#">Privacy Policy</a></p>
                {/* <p><Link to="/room_select" onClick={() => window.location.refresh()}>Edit Rooms Booking</Link></p> */}
                <p><Link to="/room_select">Edit Rooms Booking</Link></p>
              </p>
              {/* <p>
              <Checkbox onChange={this.onChangeCheckbox} className="t_d_style">
                I agree to the Terms & Coditions and Privacy Policy.
              </Checkbox>
            </p> */}

            </div>
          </Card>
        </Affix>
      </div>
    )
  }
}


export default withRouter(PersonalBookingDetail);
