import React, { Component } from 'react';
import { Statistic, Modal, Result, Button } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { config } from 'rxjs';
const { Countdown } = Statistic;
const deadline = Date.now() + 1 * 1 * 30 * 12 * 2 + 1000 * 30;


class TimeCountdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMadal: false,
      count: 10,

    }

  }

  componentWillUnmount() {
    clearInterval(this.inter);
    clearTimeout(this.settime);
  }


  onFinish = () => {
    console.log('finished!');
    this.setState({ isMadal: true, })

    var that = this;
    that.inter = setInterval(() => {
      this.setState((prevState) => ({ count: prevState.count - 1 }));
    }, 1000);

    that.settime = setTimeout(() => {
      this.setState({
        isMadal: false,
      })
      that.props.history.push({
        pathname: '/'
      });
      window.location.reload();
    }, 10000);
  }

  continue = () => {
    this.setState({
      isMadal: false,
    });
    clearInterval(this.inter);
    clearTimeout(this.settime);

  };

  backtToHome = () => {
    this.setState({
      isMadal: false,
    });
    var that = this;
    that.props.history.push({
      pathname: '/'
    });
    window.location.reload();
  };


  render() {
    console.log(this.props.drawer, 'drawer')
    return (
      <div className="TimeCountdown">
        <Countdown value={deadline} onFinish={this.onFinish} />
        <Modal
          visible={this.state.isMadal}
          header={null}
          closable={false}
          onOk={null}
          onOk={this.continue}
          onCancel={this.backtToHome}
          destroyOnClose={true}
          className="continue"
          maskClosable={false}
        >
          <Result
            status="warning"
            title='Want to continue this page ?'
          />
          Back to home in {this.state.count}s...
            </Modal>


      </div>
    )
  }
}


export default withRouter(TimeCountdown);
