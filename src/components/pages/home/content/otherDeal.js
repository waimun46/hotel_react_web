import React, { Component } from 'react';
import {Card, Collapse,} from 'antd';
import { summary } from '../../../json';

const { Panel } = Collapse;

function callback(key) {
  console.log(key);
}



class OtherDeals extends Component {
  
  render() {
    return (
      <div className="OtherDeals">
        <Card title="OTHER DEALS" className="card_warp other_warp_deals">
          {
            summary[0].other.map((item, i) => {
              return (
                <div className="summary_price" key={i}>
                  <p>
                    <span className="content">
                      <img src={item.logo} alt="logo" /> </span>
                    <span className="price"><small>MYR</small>{item.price}</span>
                  </p>
                </div>
              )
            })
          }

          {/* <Collapse onChange={callback} expandIconPosition="right" className="view_all">
            <Panel header="VIEW ALL DEALS" key="1">
              {
                summary[0].other.map((item, i) => {
                  return (
                    <div className="summary_price" key={i}>
                      <p>
                        <span className="content">
                          <img src={item.logo} alt="logo" /> </span>
                        <span className="price"><small>MYR</small>{item.price}</span>
                      </p>
                    </div>
                  )
                })
              }
            </Panel>
          </Collapse>

          <p className="price_notice">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          </p> */}

        </Card>
      </div>
    )
  }
}


export default OtherDeals;
