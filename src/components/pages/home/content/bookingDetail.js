import React, { Component } from 'react';
import { Card, Collapse, Affix } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { Button, Icon } from 'semantic-ui-react'


class BookingDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sticky: 10,

    }
  }



  render() {
    const { sticky } = this.state;
    const summarys = this.props.summary
    console.log(this.props.data, '------summary')

    return (
      <div className="BookingDetails">
        <Affix offsetTop={sticky}>
          <Card title="BOOK DETAIL" className="card_warp "
            // actions={[<Link to="/personal_detail" onClick={() => window.location.refresh()}>
            actions={[<Link to="/personal_detail">
              <div className="footer">BOOK NOW</div></Link>]}>
            {
              this.props.data.map((item, index) => {
                return (
                  <div className="booking_detail">
                    <p><span className="content_title">Duration of Stay</span><span className="content_list">{item.duration}</span></p>
                    <p><span className="content_title">Room Type</span><span className="content_list">{item.type}</span></p>
                    <p><span className="content_title">Bed Type</span><span className="content_list">{item.bed_type}</span></p>
                    <p><span className="content_title">No of Rooms</span><span className="content_list">{item.room}</span></p>
                    <p><span className="content_title">Guest No</span><span className="content_list">{item.guest}</span></p>
                    <p><span className="content_title">Price</span><span className="content_list">{item.price}</span></p>
                    {
                      index > 0 ? (
                        <Button icon onClick={this.props.onClickRemove(index)} className="remove_btn">
                          <Icon name='delete' />
                        </Button>
                      ) : (
                          null
                        )
                    }
                    <div className="clear"></div>
                  </div>
                )
              })
            }



            {/* <p><span className="content_title">Duration of Stay</span><span className="content_list">{summarys.duration}</span></p>
              <p><span className="content_title">Room Type</span><span className="content_list">{summarys.room_type}</span></p>
              <p><span className="content_title">Bed Type</span><span className="content_list">{summarys.bed_type}</span></p>
              <p><span className="content_title">No of Rooms</span><span className="content_list">{summarys.no_room}</span></p>
              <p><span className="content_title">Guest No</span><span className="content_list">{summarys.guest}</span></p>
              <p><span className="content_title">Price</span><span className="content_list">{summarys.price}</span></p> */}


            {/* <div className="facilitie">
               <div className="other_warp">
                <p className="content_title">Other charges : </p>
                <div className="other_detail">
                  <p><span className="content_title">Add Bed</span><span className="content_list">{summarys.add_bed}</span></p>
                  <p><span className="content_title">Add Breakfast</span><span className="content_list">{summarys.add_breakfast}</span></p>
                </div>
              </div> 
            </div> */}
            <div className="summary_price">
              <p><span className="content">SUMMARY</span><span className="price"><small>RM</small>{summarys.total}</span></p>
            </div>
            <div className="notice">
              <p className="content">
                Price Included: 6% Service Tax, if any
                Price Not Included: RM 10 Tourism Tax per room per night (foreign guest only, not applicable to Malaysians),
                RM 2 Heritage Tax per room per night (if any)
                Full Payment Required Upon Booking.
              <p className="cancel"><a href="#">Cancellation Policy</a></p>
                <p><a href="#">Privacy Policy</a></p>
              </p>
            </div>
          </Card>
        </Affix>
      </div>
    )
  }
}


export default withRouter(BookingDetails);
