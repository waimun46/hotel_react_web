import React, { Component } from 'react';
import { Statistic, Card, Modal, Result } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { summary } from '../../../json';
import TimeCountdown from './timeCountdown';
import PersonalBookingDetail from './personalBookingDetail';



class PersonalSummary extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }



  render() {
 
    return (
      <div className="Summery PersonalSummary">
        <TimeCountdown/>
        <PersonalBookingDetail submitFunction={this.props.submitFunction}/>

      </div>
    )
  }
}


export default withRouter(PersonalSummary);
