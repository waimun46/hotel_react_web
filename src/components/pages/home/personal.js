import React, { Component , useReducer } from 'react';
import { Input, Select, Collapse , Checkbox, Button, Drawer, Alert } from 'antd';
import PersonalSummary from './content/personalSummary';
import PersonalBookingDetail from './content/personalBookingDetail';
import TimeCountdown from './content/timeCountdown';
import { country, designations } from '../../json';
import $ from 'jquery'
import Menus from '../menu';

const { Option } = Select;
const { Panel } = Collapse;
const ButtonGroup = Button.Group;



class PersonalDetail extends Component {
  
  constructor(props){
   
    super(props);
    this.state = {
      breakfast: 0,
      bed: 0,
      visibleDrawer: false,
      placement: 'bottom',
      designation:'',
      countrys: '',
      value:'',
      hightFloor: '',
      terms_coditions: '',
      windowHeight: undefined,
      windowWidth: undefined,
      errorValidation: { first_name:'',last_name:'',phone: '',passport:'',email: ''},
      personalData:{first_name:'',last_name:'',phone: '',passport:'',email: ''}


    }


  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }


  /*************************************************  window width function **************************************************/

  handleResize = () => this.setState({
    windowHeight: window.innerHeight,
    windowWidth: window.innerWidth
  });
  
   
  /************************************************* Drawer function *******as*******************************************/

  showDrawer = () => {
    console.log('-------function')
    this.setState({
      visibleDrawer: true,
    });
  };

  onCloseDrawer = () => {
    this.setState({
      visibleDrawer: false,
    });
  };

  onChangeDrawer = e => {
    this.setState({
      placement: e.target.value,
    });
  };
  /************************************************* count breakfast **************************************************/

  increaseBreakfast = () => {
    let breakfast = this.state.breakfast + 1;
    if (breakfast > 10) {
      breakfast = 10;
    }
    this.setState({ breakfast });
  };

  declineBreakfast = () => {
    let breakfast = this.state.breakfast - 1;
    if (breakfast < 0) {
      breakfast = 0;
    }
    this.setState({ breakfast });
  };
  /************************************************* count bed **************************************************/

  increaseBed = () => {
    let bed = this.state.bed + 1;
    if (bed > 10) {
      bed = 10;
    }
    this.setState({ bed });
  };

  declineBed = () => {
    let bed = this.state.bed - 1;
    if (bed < 0) {
      bed = 0;
    }
    this.setState({ bed });
  };

  /************************************************* handleSelect function **************************************************/

  handleChangeSelectTitle = (value) => {
    console.log(`selected ${value}`);
    this.setState({ 
      designation: value,
    });
  }
  
  handleChangeSelectCountry = (value) => {
    console.log(`selected ${value}`);
    this.setState({ 
      countrys: value,
    });
  }

  // handleChangeInput = (e) => {
  //   this.setState({ [e.target.name]: e.target.value });
  //  }

   handleChangeInput(index,name,value){
     this.state.personalData[name] = value
    this.setState({ personalData: this.state.personalData});
   }

  onChangeCheckBox  = (e) => {
    //console.log(`checked = ${e.target.checked}`);
    this.setState({
      hightFloor: e.target.checked,
      terms_coditions: e.target.checked
    })

  }





  /************************************************* submit function **************************************************/
  
  submitForm = (e) => {
    e.preventDefault()
    const { designation,countrys, hightFloor ,breakfast, bed, terms_coditions,errorValidation,personalData } = this.state;

    let outputJson = {
      designation: designation,
      personalData: personalData,
      countrys: countrys,
      highFloor: hightFloor === true ? 'Yes' : 'No',
      add_breakfast: breakfast,
      add_bed: bed,
      terms_coditions: terms_coditions === true ? 'agree' : 'no agree',
      

    }
    console.log(outputJson,'-------outputJson');

    if(designation === "" || countrys === "" || personalData.first_name === "" || personalData.last_name === "" || 
      personalData.phone === "" ||   personalData.passport === "" ||  personalData.email === "" ||  terms_coditions === ""){

      if(designation === ""){
        $(".titleID .ant-select-selection").addClass("error_border");
        errorValidation.designation = '*Please select the designation'
        this.setState({errorValidation: errorValidation})
      }else{ 
        $(".titleID .ant-select-selection").removeClass("error_border")
        errorValidation.designation = ''
        this.setState({errorValidation: errorValidation})
      }
  
      if(personalData.first_name === ""){
       errorValidation.first_name = '*Please fill in the first name';
       this.setState({errorValidation: errorValidation})
      }else{
        errorValidation.first_name = '';
        this.setState({errorValidation: errorValidation})
      }
  
      if(personalData.last_name === ""){
        errorValidation.last_name = '*Please fill in the last name';
        this.setState({errorValidation: errorValidation})
      }else{
        errorValidation.last_name = '';
        this.setState({errorValidation: errorValidation})
      }
  
      if(countrys === ""){
        $(".last_countryID .ant-select-selection").addClass("error_border");
        errorValidation.countrys = '*Please select the country';
        this.setState({errorValidation: errorValidation})
      }else{
        $(".last_countryID .ant-select-selection").removeClass("error_border")
        errorValidation.countrys = '';
        this.setState({errorValidation: errorValidation})
      }
  
      if(personalData.phone === ""){
        errorValidation.phone = '*Please fill in the phone number';
        this.setState({errorValidation: errorValidation})
      }else{
        errorValidation.phone = '';
        this.setState({errorValidation: errorValidation})
      }
  
      if(personalData.passport === ""){
        errorValidation.passport = '*Please fill in the passport or ID number';
        this.setState({errorValidation: errorValidation})
      }else{
        errorValidation.passport = '';
        this.setState({errorValidation: errorValidation})
      }
  
      if(personalData.email === ""){
        errorValidation.email = '*Please fill in the email';
        this.setState({errorValidation: errorValidation})
      }else{
        errorValidation.email = '';
        this.setState({errorValidation: errorValidation})
      }
  
      if(terms_coditions === ""){
        $(".checkID .ant-checkbox").addClass("error_border_checkbox");
        errorValidation.terms_coditions = '*Please agree the terms & coditions';
        this.setState({errorValidation: errorValidation})
      }else{
        $(".checkID .ant-checkbox").removeClass("error_border_checkbox")
        errorValidation.terms_coditions = '';
        this.setState({errorValidation: errorValidation})
      }

    }else{
      this.props.history.push('/payment')
      
    }

  

  }


  

  render() {

    const { designation, hightFloor,terms_coditions,windowWidth, errorValidation, personalData} = this.state; 
    //console.log(this.state.windowWidth,'-----windowWidth personal')

    return (
      <div>
        <Menus/>
      
      <div className="PersonalDetail">
      
      <div className="res_warp">
        <div className="form_detail">
       
        
          <div className="header">
            <h1>Who's comming?</h1>
            <p>Guest details</p>
            <p>The infor entered will be used to add people to this reservation.</p>
          </div>
         

          <form className="form_warp">
          
            <div className="form_control country">
              <label>Title</label>
              <Select defaultValue={designation} className="titleID" onChange={this.handleChangeSelectTitle}>
                {
                  designations.map((item => {
                    return (
                      <Option value={item.designation}>{item.designation}</Option>
                    )
                  }))
                }
              </Select>
              <p className="error">{errorValidation.designation}</p>
            </div>


            <div className="form_control">
              <div className="forms">
                <label>First Name</label>
                <Input autoComplete="off"  defaultValue={personalData.first_name} placeholder="Your First Name"
                  onChange={(e) => this.handleChangeInput(0, 'first_name', e.target.value)} 
                  className={errorValidation.first_name ? 'error_border' : ''}
                 />
                <p className="error">{errorValidation.first_name}</p>
              </div>
              <div className="forms">
                <label>Last Name</label>
                <Input autoComplete="off" defaultValue={personalData.last_name} placeholder="Your Last Name"
                  onChange={(e) => this.handleChangeInput(0, 'last_name', e.target.value)} 
                  className={errorValidation.last_name ? 'error_border' : ''}
                 />
                <p className="error">{errorValidation.last_name}</p>
              </div>
              <div className="clear"></div>
            </div>

            <div className="form_control country">
              <div className="forms">
                <label>Country</label>
                <Select defaultValue={personalData.countrys} className="last_countryID"  onChange={this.handleChangeSelectCountry}>
                  {
                    country.map((item => {
                      return (
                        <Option value={item.country}>{item.country}</Option>
                      )
                    }))
                  }
                </Select>
                <p className="error">{errorValidation.countrys}</p>
              </div>
              <div className="forms">
                <label>Mobile Number</label>
                <Input autoComplete="off" defaultValue={personalData.phone} placeholder="Exp:0123456789" 
                  onChange={(e) => this.handleChangeInput(0, 'phone', e.target.value)} 
                  className={errorValidation.phone ? 'error_border' : ''}
                />
                <p className="error">{errorValidation.phone}</p>
              </div>
              <div className="clear"></div>
            </div>

            <div className="form_control">
              <label>Passport or ID number</label>
              <Input autoComplete="off" defaultValue={personalData.passport}  placeholder="Passport or ID number..."
               onChange={(event) => this.handleChangeInput(0, 'passport', event.target.value)} 
               className={errorValidation.passport ? 'error_border' : ''}
               />
              <p className="error">{errorValidation.passport}</p>
            </div>


            <div className="form_control">
              <label>Email</label>
              <Input autoComplete="off" defaultValue={personalData.email} placeholder="Exp:guest@gmail.com" 
                onChange={(e) => this.handleChangeInput(0, 'email', e.target.value)} 
                className={errorValidation.email ? 'error_border' : ''}
              />
              <p className="error">{errorValidation.email}</p>
            </div>

            <div className="form_control add_warp">
              <label>Special Request (optional)</label>
              <Collapse accordion expandIconPosition="right"  defaultActiveKey={['1']}>
                <Panel header="Have a special request? Ask, and we will 
                do its best to meet your wish.(Note that special request are not guaranteed and may incur charges)" key="1">
                  <div>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                       standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it 
                       to make a type specimen book.
                    </p>
                    <div className="add_select_warp">
                      <div className="add_select">
                        <span className="title">Hight Floor</span>
                        <span className="select">
                          <Checkbox value={hightFloor} onChange={this.onChangeCheckBox}/>
                        </span>
                      </div>
                      <div className="add_select">
                        <span className="title">Add Breakfast</span>
                        <span className="select breakfast">
                          <Button onClick={this.declineBreakfast} icon="minus" />
                          <span className="number">{this.state.breakfast}</span>
                          <Button onClick={this.increaseBreakfast} icon="plus" />
                        </span>
                      </div>
                      <div className="add_select">
                        <span className="title">Add Bed</span>
                        <span className="select breakfast">
                        <Button onClick={this.declineBed}  icon="minus" />
                          <span className="number">{this.state.bed}</span>
                          <Button onClick={this.increaseBed} icon="plus"/>
                        </span>
                      </div>
                    </div>

                  </div>
                </Panel>
              </Collapse>
            </div>

            <p>
              <Checkbox defaultValue={terms_coditions} className="checkID t_d_style"  onChange={this.onChangeCheckBox} >
                I agree to the Terms & Coditions and Privacy Policy.
              </Checkbox>
              <p className="error">{errorValidation.terms_coditions}</p>
            </p>

            

          </form>


        </div>
        </div>

        {
          windowWidth < 1000 ? (
            null
          ) : (
            <div className="desktop_view">
              <PersonalSummary submitFunction={this.submitForm} />
            </div>
          )
        }
        
        {
          windowWidth > 1000 ? (
            null
          ) : (
            <div className="mobile_view stick_bottom" >
            <Button type="primary" onClick={this.showDrawer}>
              <span className="details">
                <p>RM 180 for 2 guest</p>
                <p>See details</p>
              </span>
              <span className="time">
                <TimeCountdown/>
              </span>
            </Button>
            <Drawer
              placement={this.state.placement}
              onClose={this.onCloseDrawer}
              visible={this.state.visibleDrawer}
              height="90%"
              className="drawer_warp"
            >
              <div className="res_drawer">
                <PersonalBookingDetail submitFunction={this.submitForm} /> 
              </div>
            </Drawer>
          </div>
          )
        }
      
      </div>
      </div>
    )
  }
}


export default PersonalDetail;
