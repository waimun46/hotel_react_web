import React, { Component } from 'react';
import { Spin, Button, Icon } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import './payment.css';
import $ from 'jquery'
import Menus from '../menu';


class Payments extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {

  }


  hideSpinner = () => {
    this.setState({
      isLoading: false
    });
  };



  changeStuff() {
    const myiFrame = document.getElementById("myFrame");
    const iframeWindow = myiFrame.contentWindow;
    // const iframeDoc = myiFrame.contentDocument;
    // iframeDoc.body.style.backgroundColor = "#db2828"
    // console.log(iframeWindow.document,'---------change')

    // const childFrameObj = document.getElementById('myFrame');
    // childFrameObj.contentWindow.postMessage(1233, '*');

    // var x = document.getElementById("myFrame");
    // var y = (x.contentWindow || x.contentDocument);
    // if (y.document)y = y.document;
    // y.body.style.backgroundColor = "red";


    const currentIframeHref = new URL(document.location.href);
    const urlOrigin = currentIframeHref.origin;
    const urlFilePath = decodeURIComponent(currentIframeHref.pathname);

    // const getIframeHref = document.getElementById("myFrame").contentWindow.location.href


    console.log(urlFilePath, '---------urlFilePath')

  }




  render() {
    const { isLoading } = this.state;
    const url = this.props.location.pathname
    console.log(url, '------url')
    return (
      <div>
        <Menus />

        <div className="Payments">
          {
            isLoading ? (
              <div className="example">
                <Spin size="large" />
              </div>
            ) : null
          }

          {/* <button onClick={this.changeStuff}>changeStuff</button> */}

          <iframe
            src="https://www.goldendestinations.com"
            width="100%"
            height="700"
            onLoad={this.hideSpinner}
            frameBorder="0"
            marginHeight="0"
            marginWidth="0"
            id="myFrame"

          />

          <div className="next_btn">
            <Link to="/confirm" onClick={() => window.location.refresh()}>
              <Button type="primary" onClick={this.confrim}>
                Next
          </Button>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}


export default withRouter(Payments);
