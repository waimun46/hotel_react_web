import React, { Component } from 'react';
import { Result, Button, Card } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import './confirm.css';

class ConfirmOrder extends Component {


 
  render() {

    const url = this.props.location.pathname
    console.log(url, '------url')
    return (
      <div className="ConfirmOrder">
        <Card className="confirm_card">
          <Result
            status="success"
            title="Successfully Purchased Your Order!"
            subTitle="Order number: 2017182818828182881"
            extra={[
              <Link to="/" onClick={() => window.location.refresh()}>
                <Button type="primary" key="console">
                  Back to Home
                </Button>
              </Link>
            ]}
          />
        </Card>

      </div>
    )
  }
}


export default withRouter(ConfirmOrder);
