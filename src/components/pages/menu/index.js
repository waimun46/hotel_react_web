import React, { Component } from 'react';
import { Icon, Step } from 'semantic-ui-react';
import logo from '../../assets/logo/logo.png'
import './menu.css';
import { Link, withRouter } from 'react-router-dom';


class Menus extends Component {

  constructor(props) {
    super(props);
    this.goBack = this.goBack.bind(this);
  }

  goBack() {
    this.props.history.goBack();
  }

  render() {
    const currentPath = window.location.pathname;
    console.log(currentPath, '----------currentPath');


    const steps = [
      {
        key: 'room',
        icon: 'hotel',
        title: 'Room',
        active: currentPath === '/room_select' ? true : false,
        // active: currentPath === '/' ? true : false || currentPath === '/room_select' ? true : false ||
        //   currentPath === '/personal_detail' ? true : false,
        disabled: currentPath !== '/room_select' ? true : false,
        description: 'Choose your room options',
        // onClick: this.goBack,
      },
      {
        key: 'personal_detail',
        active: currentPath === '/personal_detail' ? true : false,
        disabled: currentPath !== '/personal_detail' ? true : false,
        icon: 'dollar',
        title: 'Personal Infor',
        description: 'Enter personal information',
        // onClick: this.goBack,
      },
      {
        key: 'payment',
        active: currentPath === '/payment' ? true : false,
        disabled: currentPath !== '/payment' ? true : false,
        icon: 'checkmark',
        title: 'Payment',
        description: 'Enter payment information',
        // onClick: this.goBack,
      },
      // {
      //   key: 'confirm',
      //   active: currentPath === '/confirm' ? true : false,
      //   disabled: currentPath !== '/confirm' ? true : false,
      //   icon: 'checkmark',
      //   title: 'Confirm Order',
      //   description: 'Confirm your order',
      // },
    ]


    return (
      <div className="Menu">
        <div className="logo">
          <img src={logo} alt="alt" />
        </div>
        <div className="menu_warp">
          <Step.Group items={steps} />
        </div>
      </div>
    )
  }
}


export default withRouter(Menus);
